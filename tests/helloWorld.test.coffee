{ namedSequenceOf, char, str } = require "arcsecond"
{ tokenizer, rawcmd } = require "../src/index"
{ formatWithOptions } = require "util"
# YAML = require "yaml"

require("chai").should()

###
describe "tautology", ->
  it "should be a tautology", ->
    "tautology".should.be.equal "tautology"
    
describe "hello world", ->
  it "should not throw an error", ->
    (-> console.log "hello world").should.not.throw()

describe "namedSequenceOf", ->
  it "should do something I guess", ->
    nsoParser = namedSequenceOf [
      ["_", char 'h']
        ["_", char 'e']
        ["hmmm", str 'll']
        ["_", char 'o']
    ]
    console.log nsoParser.run "hello"
###

describe "tokenizer", ->
  it "should log something interesting", ->
    console.log formatWithOptions {
      colors: yes
      depth: 100
    }, "\x1b[34m'#{rawcmd}'\x1b[0m becomes:\n\x1b[1mParserResult\x1b[0m", tokenizer.run rawcmd
