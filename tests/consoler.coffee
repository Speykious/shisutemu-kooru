{ logenize }   = require "../src/helpers"
Syscall        = require "../src/Syscall"
SacredArts     = require "../src/SacredArts"

###
What info is necessary to create a command?
Let's try to make one...
###
cipGenerate =
  name: "generate"
  名: "ジェネレート"
  args:
    element:
      名: "エレメント"
      position: "end"
      type: "word"
      first: yes
    shape:
      名: "シェープ"
      position: "end"
      type: "wordlist"
      enum: ["arrow", "sword", "vortex"]
      optional: yes
    to:
      名: "テゥー"
      position: "between"
      type_left: "word"
      type_right: "word"
    id:
      名: "ｉｄ"
      position: "start"
      type: "digits"

cipVerifyPlayer =
  name: "verify-player"
  名: "ベリファイ"
  args:
    id:
      名: "ｉｄ"
      position: "start"
      type: "snowflake"
    shape:
      名: "シェープ"
      position: "end"
      type: "wordlist"
      enum: ["alpha-tester", "beta-tester", "simp"]
    email:
      position: "start"
      type: "word"

rawcmds = [
  "SYSTEM CALL: GENERATE thermal ELEMENT, self TO front, ID 001"
  "SYSTEM CALL: generate thermal element, value to 012345678901234567, id 001"
  "sc: verify-player, id 012345678901234567, simp alpha-tester shape, not-an-email email"
  "sc: verify-player, id 012345678901234567, beta-tester shape, email something@somewhere.com"
  "sc: verify-player with id 012345678901234567, beta-tester shape, email something@somewhere.com"
  "System-Call: SEND MESSAGE, TARGET-ID 69420, CONTENT 'Haha string go brrr'"
  "system call: this sentence doesn't make any sense"
  "System call: what about, trailing commas,"
  "System call: , and what about, starting with a comma"
  # It seems that japanese parsing doesn't work very well
  #"システム・コール：　ジェネレート　サーマル　エレメント, セルフ　テゥー　フロント, ＩＤ　001"
]

generatedTests = [
  "SYSTEM CALL: GENERATE THERMAL ELEMENT, VORTEX ARROW SHAPE, SOMETHING TO 012345678901234567, ID 98765432109876543"
  "SYSTEM CALL: GENERATE THERMAL ELEMENT, SHAPE, SOMETHING TO 012345678901234567, ID 98765432109876543"
  "SYSTEM CALL: GENERATE ELEMENT, SHAPE, SOMETHING TO 012345678901234567, ID 98765432109876543"
  "SYSTEM CALL: GENERATE THERMAL ELEMENT, SHAPE, SOMETHING 012345678901234567, ID 98765432109876543"
]

intoSyscall = (info) -> new Syscall info, (r) ->
  console.log "Got result!"
  return r
sacredArtsProto = new SacredArts [cipGenerate, cipVerifyPlayer].map intoSyscall

rawcmds.map logenize sacredArtsProto

###
rawcmds.map logenize tokenizer
parser = cmdParser cmdInfoPrototype

generatedTests.map logenize pipeResult [
  tokenizer.map (r) -> r.arts
  parser
]
###
