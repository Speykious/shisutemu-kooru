# システムコール [![NPM version](https://badge.fury.io/js/shisutemu-kooru.svg)](https://npmjs.org/package/shisutemu-kooru)

> SYSTEM CALL: GENERATE COMMAND MANAGER ELEMENT, SWORD ART ONLINE ALICIZATION SHAPE; RELEASE

システムコール (shisutemu-kooru) is a command manager/parser inspired from Sword Art Online Alicization system calls. Great for weeb nerds (like me).

***

## Installation

```bash
# using npm
npm install --save shisutemu-kooru
# using yarn
yarn add shisutemu-kooru
```

## Usage

(Coming soon™️)

## License

This program uses the [MIT license](./LICENSE). © 2020 [Speykious](https://gitlab/Speykious/)
