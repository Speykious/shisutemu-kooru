{ strparse } = require "arcthird"
cmdParser    = require "./cmdParser"

class Syscall
  constructor: (cmdInfo, @onSuccess) ->
    @name = cmdInfo.name
    @名   = cmdInfo.名
    @argParser = cmdParser cmdInfo.args

  evalArgs: (tokenArgs) ->
    { result, isError, error } = @argParser.parse tokenArgs
    return if isError then { isError, error } else { isError, result: @onSuccess result }

module.exports = Syscall
