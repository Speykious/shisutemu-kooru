{ formatWithOptions } = require "util"
{ blue, bold, cyan } = require "ansi-colors-ts"
{
  regex
  digits
  between
  strparse
  optionalWhitespace
  Parser
  PStream
} = require "arcthird"

class TokenPStream extends PStream
  length: () -> @structure.length
  elementAt: (i) ->
    try
      @structure[i]
    catch e
      null

nextToken = new Parser (s) ->
  unless s.target instanceof TokenPStream
    throw new TypeError "nextToken expects a TokenPStream instance as target, got #{typeof s.target} instead"
  if s.isError then return s
  { index, target } = s
  targetLength = target.length()
  if index < targetLength
    res = target.elementAt index
    return s.update res, index + 1
  else s.errorify "ParseError (position #{index}): Expecting token, got end of input"

ofType = (t) -> nextToken.filter (({ type }) -> type is t), ({ target, index }) ->
  "ParseError (position #{index}): Expecting token of type '#{t}', got #{
    if index + 1 < target.length()
    then "token of type '#{(target.elementAt index + 1).type}'"
    else "end of input"}"
notOfType = (t) -> nextToken.filter (({ type }) -> type isnt t), ({ target, index }) ->
  "ParseError (position #{index}): Expecting token of any type but '#{t}'"

t_word      = ofType "word"
t_string    = ofType "string"
t_snowflake = ofType "snowflake"
t_sep       = ofType "sep"
t_notSep    = notOfType "sep"

t_wordExactly = (s) -> t_word.filter ((w) -> w is s), ({ target, index }) ->
  "ParseError (position #{index}): Expecting word to have value #{s}, got #{
    if index + 1 < target.length()
    then "token of value '#{(target.elementAt index + 1).value}'"
    else "end of input"}"

join = (sep) -> (arr) -> arr.join sep

wordRegex = /^[^,"\s\n'][^,"\s\n]*/i
word = regex wordRegex
  .errorMap ({ target, index }) ->
    "Expected word, got #{
      if index < target.length()
      then "'#{target.getChar index}...'"
      else "end of input"}"
  .map (result) -> result.toLowerCase()

snowflakeRegex = /^\d{17,18}/
snowflake = regex snowflakeRegex
  .errorMap ({ target, index }) ->
    "Expected snowflake, got #{
      if index < target.length()
      then "'#{target.getChar index}...'"
      else "end of input"}"

prefix = regex /^(SYSTEM[\s-]CALL|SC|システム・?コール)\s*(:|：)\s*/i

ignoreSpaces = between(optionalWhitespace)(optionalWhitespace)
spacedWord = ignoreSpaces word
spacedSnowflake = ignoreSpaces snowflake
spacedDigits = ignoreSpaces digits



# Logging stuff
colmat = (args...) -> (formatWithOptions { colors: yes, depth: 100 }, "", args...).slice 1

printState = ({ isError, result, error }) ->
  if isError
  then "\x1b[31mError ->\x1b[0m #{colmat error}"
  else "\x1b[33mResult ->\x1b[0m #{colmat result}"

logenize = (sacredArts) -> (rawcmd) ->
  console.log """
              #{blue "'#{rawcmd.replace /'/g, bold cyan "\\'"}'"}
              #{(sacredArts.eval rawcmd).map printState}\n
              """

module.exports = {
  TokenPStream
  nextToken
  ofType
  notOfType
  t_word
  t_string
  t_snowflake
  t_sep
  t_notSep
  t_wordExactly
  spacedSnowflake
  ignoreSpaces
  spacedDigits
  spacedWord
  wordRegex
  snowflake
  prefix
  word
  join

  colmat
  printState
  logenize
}
