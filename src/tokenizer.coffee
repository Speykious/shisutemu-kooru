{
  char
  regex
  digits
  endOfInput
  anyCharExcept
  optionalWhitespace
  takeRight, takeLeft
  namedSequenceOf
  many, choice
  between
} = require "arcthird"
{ TokenPStream, word, snowflake, prefix, join } = require "./helpers"

escaped = (c) -> (takeRight char '\\') char c
quoted = (quote) ->
  qc = char quote
  return between(qc)(qc) (many choice [(escaped quote), (anyCharExcept qc)]).map join ''
squoted = quoted "'"
dquoted = quoted '"'

word_t      = word.map (result) -> { type: "word",   value: result }
string_t    = (choice [squoted, dquoted]).map (result) -> { type: "string", value: result }
digits_t    = digits.map (result) -> { type: "digits", value: result }
comma_t     = (char ',').map (result) -> { type: "sep" }
snowflake_t = snowflake.map (result) -> { type: "snowflake", value: result }
anyToken = choice [snowflake_t, digits_t, word_t, string_t, comma_t]
endOfTokens = endOfInput.errorMap ({ target, index }) ->
  "Unexpected token at index #{index}: '#{target.getUtf8Char index, target.getCharWidth index}...'"

tokenizer = (takeLeft many takeRight(optionalWhitespace)(anyToken)) endOfTokens
            .map (arr) -> new TokenPStream arr

module.exports = tokenizer
