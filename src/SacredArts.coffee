{ strparse, regex, choice, takeRight, optionalWhitespace } = require "arcthird"
{ prefix, word, ignoreSpaces } = require "./helpers"
tokenizer = require "./tokenizer"
Syscall = require "./Syscall"

class SacredArts
  constructor: (syscalls, prefixRegex) ->
    unless syscalls instanceof Array
      throw new TypeError "The SacredArts contructor expects an array"
    for syscall in syscalls
      unless syscall instanceof Syscall
        throw new TypeError "The SacredArts constructor expects an array of instances of Syscall"

    @syscalls = syscalls
    @prefix = if prefixRegex then regex prefixRegex else prefix

  systemcall: (name) ->
    for syscall in @syscalls
      if name is syscall.name or (syscall.名 and name is syscall.名)
        return syscall
    return null
  
  eval: (s) ->
    { isError, index, result } = (strparse @prefix) s
    if isError then return null
    return s[index..].split "\n"
    .map ((cmd) ->
      { index, result, isError, error } = (strparse ignoreSpaces word) cmd
      if isError then return { isError, error }
      syscall = @systemcall result
      unless syscall then return { isError: yes, error: "Command '#{result}' not found" }
      { result, isError, error } = (strparse tokenizer) cmd[index..]
      if isError then return { isError, error }
      return syscall.evalArgs result).bind @

module.exports = SacredArts
