{
  str
  many
  sepBy
  choice
  namedSequenceOf
  Parser
} = require "arcthird"
{
  word
  wordRegex
  ignoreSpaces
  t_word
  t_snowflake
  t_digits
  t_strings
  t_sep
  t_notSep
  t_wordExactly
  printState
} = require "./helpers"

parseArgRemaining = (info, remaining) ->
  switch info.type
    when "wordlist"
      if info.enum
        errors = []
        thereIsAnError = no
        for token in remaining
          unless token.value in info.enum
            errors.push "'#{token.value}' is not in #{JSON.stringify info.enum}"
            thereIsAnError = yes
        if thereIsAnError then return { isError: 2, errors }
      return { isError: 0, result: remaining.map ({ value }) -> value }
    when "word", "snowflake", "digits", "string"
      if remaining.length isnt 1
        return { isError: 2, errors: ["Expected a single '#{info.type}' token, got several tokens"] }
      token = remaining[0]
      unless token.type is info.type
        return { isError: 2, errors: ["Expected a single '#{info.type}' token, got '#{token.type}'"] }
      if info.enum
        errors = []
        thereIsAnError = no
        unless token.value in info.enum
          errors.push "'#{token.value}' is not in #{JSON.stringify info.enum}"
          thereIsAnError = yes
        if thereIsAnError then return { isError: 2, errors }
      return { isError: 0, result: token.value }
    else throw new TypeError "Invalid type property '#{info.type}'"

parseArg = (name, info) ->
  unless wordRegex.test name
    throw new TypeError "The name of the argument should be a valid word (regex #{wordRegex})"
  
  return (tokens) ->
    foundIndex = tokens.findIndex ({ type, value }) -> type is "word" and value is name
    if foundIndex is -1 then return {
      name, isError: 1
      errors: ["Cannot find the argument name '#{name}' anywhere"]
    }
    position = info.position or "start"
    switch foundIndex
      when 0
        return { name, (parseArgRemaining info, tokens[1..])... }
      when tokens.length - 1
        return { name, (parseArgRemaining info, tokens[...-1])... }
      else
        # hehehe
        rem = parseArgRemaining {
          info..., type: info.type_left or info.type
        }, tokens[...foundIndex]      # remainingLeft
        ram = parseArgRemaining {
          info..., type: info.type_right or info.type
        }, tokens[(foundIndex + 1)..] # remainingRight
        if rem.isError or ram.isError
          errors = []
          if rem.isError then errors = [errors..., rem.errors...]
          if ram.isError then errors = [errors..., ram.errors...]
          return { name, isError: (Math.max rem.isError, ram.isError), errors }
        else return { name, isError: 0, result: { left: rem.result, right: ram.result } }

bestErrors = (errors) ->
  maxval = 0
  for [_n, _e, val] in errors
    if val > maxval then maxval = val
  bests = errors.filter ([_n, _e, val]) -> val is maxval
          .map ([n, es, _val]) -> if es.length is 1 then [n, es[0]] else [n, es]

  return Object.fromEntries bests

cmdParser = (args) ->
  unless args and typeof args is "object"
    throw new TypeError "args should be an object"

  entries = Object.entries args
  entryParsers = entries.map ([name, info]) -> parseArg name, info

  argParser = (sepBy t_sep) many t_notSep
    .map (tss) ->
      tss = tss.filter (ts) -> ts.length > 0
      Object.fromEntries tss.map (ts) ->
        possibles = entryParsers.map (f) -> f ts
        rights = possibles.filter (poss) -> not poss.isError
                .map ({ name, result }) -> [name, result]
        wrongs = possibles.filter (poss) -> poss.isError
                .map ({ name, isError, errors }) -> [name, errors, isError]

        if rights.length
          return if rights.length is 1
          then rights[0]
          else ["ambiguous", rights]
        else return ["error", { token: ts, errors: bestErrors wrongs }]
    .filter ((o) -> not o.error), ({ result }) -> result.error

  return argParser

module.exports = cmdParser
